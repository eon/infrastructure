# Contribuer à markas.fr

Le dépot contenant le code de l'infrastructure markas.fr est
<https://framagit.org/markas/infrastructure>

Ce dépot contient également des
[tickets](https://framagit.org/markas/infrastructure/-/issues)
décrivant les prochaines tâches à réaliser.


## Prérequis

L'infrastructure repose fortement sur l'outils
[Nix](https://nixos.org/nix/) et la distribution NixOS. Pour
contribuer, il faut donc installer le gestionnaire de paquet Nix. Ce
gestionnaire de paquet fonctionne sur toutes les distributions en
n'entre pas en conflit avec les paquets de votre distribution.

Pour installer Nix:

    curl https://nixos.org/nix/install | sh

Si besoin, le [manuel Nix](https://nixos.org/nix/manual/#chap-quick-start)
fournit plus d'information et d'options d'installation.


## Essayer le serveur localement

Il est possible de construire et démarrer une machine virtuelle
correspondant à la configuration du serveur. Cette machine peut être
utilisée pour tester un changement ou étudier la configuration du
serveur.

Pour construire et démarrer cette machine:

    ./start-vm

Note: cette machine virtuelle utilise QEMU. Il est vivement conseillé
d'activer KVM afin de profiter de l'accéleration matérielle fournie
par le processeur. La documentation Ubuntu suivante fournies les
informations nécessaire à l'activation de KVM
<https://doc.ubuntu-fr.org/kvm>.

Une fois la machine virtuelle démarrée, il est possible de
s'authentifier avec le compte `root` et un mot de passe vide.

Les services suivant sont accessibles directement depuis la machien
hôte:

- graphana: <http://localhost:3000> (utiliser le username `admin` et
  le password `admin`)
- prometheus server: <http://localhost:9090>
- prometheus node exporter: <http://localhost:9100>
- ssh: `ssh -p 2222 root@localhost`
- nextcloud: <http://localhost:30443>. Plusieurs comptes admin sont
  créés dont le compte admin (username `admin` password `admin`).

Note: La machine virtuelle ne démarre pas si les ports 3000, 9090,
9100 de la machine hôte sont utilisés.

## Tester un changement ou une Merge Request

Des [https://nixos.org/nixos/manual/index.html#sec-nixos-tests](tests
NixOS) sont disponibles pour valider certaines fonctionnalités du
serveur. Pour jouer ces tests:

    nix-build -A tests

La documentation
[https://nixos.org/nixos/manual/index.html#sec-nixos-tests](tests
NixOS) explique également comment jouer ces tests intéractivement.

## Soumettre une modification du serveur

Le serveur applique toute les minutes la configuration définie dans la
branche master de ce dépot. Pour appliquer une modification, il faut
donc soumettre une Merge Request (MR) à déstination de la branche
master. Une fois que cette MR est mergée, le serveur se met à jour une
minute plus tard.

Lorsque ce dépot est récupéré, la signature du commit `HEAD` est
vérifiée avec les clés publiques contenues dans le repertoire
`./keys`. La configuration est alors uniquement déployée si la
signature est valide. Cela permet de s'assurer qu'un attaquant ne
puisse pas compromettre le dépot de configuration.

## Contribuer à la documentation

[La
documentation](https://framagit.org/markas/infrastructure/-/blob/master/docs),
est écrite en Markdown ou ReST et rendu par Sphinx. Pour générer le
site statique:

    nix-build -A documentation
    firefox ./result/index.html

Il suffit ensuite modifier un fichier et relancer cette commande. Une
Merge Request peut ensuite être créée avec les modifications.
