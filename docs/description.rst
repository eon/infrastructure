Description de l'infrastructure
-------------------------------

L'infrastructure est actuellement composé d'un serveur.

Environement
************

Hébergement
~~~~~~~~~~~

Le serveur est hébergé par Kimsufi à Paris.

DNS
~~~

Le nom de domaine markas.fr est géré par Gandi.

Les services d'infrastructures
******************************

Afin de faire fonctionner les services utilisateurs, des services
d'infrastructures sont nécessaires.


Mode de déploiement
~~~~~~~~~~~~~~~~~~~

La configuration du serveur est décrite dans le repository
|repository|. Toutes les minutes, un service systemd récupère le
contenu de ce repository sur le serveur et applique les potentiels
changements.

Avant d'appliquer un changement, le serveur vérifie que le commit HEAD
de la branche master a bien éé signé par une personne autorisée.


Monitoring et alerting
~~~~~~~~~~~~~~~~~~~~~~

Nous utilisons Prometheus et Grafana.

Logs
~~~~

Les logs du serveur sont exposés par
[systemd-journal-gatewayd](https://www.freedesktop.org/software/systemd/man/systemd-journal-gatewayd.service.html). Ces
logs ne sont actuellement ni exportés, ni sauvegardés.

VPN
~~~

Un VPN est utilisé pour permettre aux membres d'accéder à des données
privées, telles que les logs. Le VPN utilisé est `Wireguard
<https://www.wireguard.com/>`_.


Revisions de markas
*******************

La revision de actuellement déployée est |markas-revision|_, qui est
basée sur la release NixOS |nixpkgs-branch| en revision
|nixpkgs-revision|_.
