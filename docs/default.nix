{ pkgs, nixpkgsUrl, nixpkgsBranch, nixpkgsRev }:

let
  markasRev = pkgs.lib.commitIdFromGitRepo ./../.git;

  json = pkgs.writeTextFile {
    name = "documentation-infra.json";
    text = builtins.toJSON {
      inherit nixpkgsUrl nixpkgsBranch nixpkgsRev markasRev;
    };
  };

in
pkgs.stdenv.mkDerivation {
  name = "markas-documentation";
  src = pkgs.lib.sourceByRegex ./. ["conf.py" "Makefile" ".*rst$" ".*md$"];
  buildInputs = [(
    pkgs.python3.withPackages(p: [
      p.sphinx
      p.recommonmark p.sphinx_rtd_theme]))
  ];
  buildPhase = ''
    cp ${json} documentation-infra.json
    SPHINXOPTS=-W make html
  '';
  installPhase = ''
    cp -r build/html $out
  '';
}
