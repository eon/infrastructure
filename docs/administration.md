# Administration de l'infrastructure

Cette section documente les opérations d'administration de
l'infrastructure.

## Mise à jour de la distibution NixOS

Le serveur est basé sur la dernière release NixOS. Pour suivre les
mise à jour, nous utilisons
[Niv](https://github.com/nmattia/niv). Pour suivre les mises à jour, il
faut donc faire:

    niv update nixpkgs

Il faut ensuite lancer les tests et commiter les fichiers modifiés.

Cette opération est actuellement manuelle mais pourra être effectuée
par la CI.


## Procédure d'installation de NixOS sur Kimsufi

Voir https://lewo.abesis.fr/posts/2019-12-01-install-nixos-on-kimsufi.html


## Signature des commits

Pour n'avoir que des commits signés et ne pas faire confiance à
Gitlab, les MR sont mergées en "fast forward".


## Secrets

Le module ../modules/keys.nix permet de définir des secrets utilisés
par d'autres modules. Chaque secret est spécifié dans un fichier. La
création de ce fichier est laissé à l'utilisateur (généralement créé
via ssh ou scp).

Lorsque qu'un secret est défini dans la configuration, un service
systemd est créé pour observer le fichier contenant ce secret. Ce
service systemd peut alors être utilisé comme dépendance d'un module
ayant besoin du secret contenu dans ce fichier.

Le test ../tests/keys.nix illustre cette fonctionnalité. Dans ce test,
un secret `foo` est utilisé par un service systemd `test`. Tant que le
fichier `/run/keys/foo` n'est présent, systemd bloque le démarrage du
service `test`. Une fois le fichier `/run/keys/foo` créé, le service
`test` est démarré.


## VPN

Un VPN est utilisé pour accéder certains des services de
l'infrastructure:

- journald: <vpn.markas.fr:19531>
- graphana: <vpn.markas.fr:3000>
- prometheus: <vpn.markas.fr:9090>
- prometheus node exporter: <vpn.markas.fr:9100>
- prometheus alertmanager: <vpn.markas.fr:9093>

Ce VPN est basé sur Wireguard. La clé publique du VPN est
`jngKolQu7KZUw+vqKUjDRHF3C9PO8nnly69lPg8pbX0=`.

Un exemple de configuration cliente (pour NixOS) est disponible dans
le [test](https://framagit.org/markas/infrastructure/-/blob/master/tests/vpn.nix). Cette configuration est à adapter en
fonction de votre distribution. IL faut également soumettre une
demande fusion avec votre clé Wireguard publique.

Note: le VPN rend votre machine utilisateur accessible depuis le
serveur. Il est donc préférable d'avoir un firewall!
