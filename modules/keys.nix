# This module is in charge of managing secret keys. For each key, a
# systemd service is created an wait for the existence of this
# file. This systemd service can them be used as the dependency of the
# systemd service that needs this secret. It will then wait until the
# secret exists.
#
# A largs part of this module comes from the key managment module of
# the Nixops tool. See
# https://github.com/NixOS/nixops/blob/c9c8adecb77fbc22067e1cf73fcba666e243ae5e/nix/keys.nix
{ config, pkgs, lib, ... }:

with lib;

let
  keyOptionsType = types.submodule ({ config, name, ... }: {
    options.path = mkOption {
      type = types.path;
      default = "${config.destDir}/${name}";
      internal = true;
      description = ''
        Path to the destination of the file
        
        This file can be located either in a persistent location, or
        volatile. For instance, the `/run/keys` location is non
        persistent across reboot. This means the secret needs to be
        recreated when the machine reboots.
      '';
    };

    options.user = mkOption {
      default = "root";
      type = types.str;
      description = ''
        The user which will be the owner of the key file.
      '';
    };

    options.group = mkOption {
      default = "root";
      type = types.str;
      description = ''
        The group that will be set for the key file.
      '';
    };

    options.permissions = mkOption {
      default = "0600";
      example = "0640";
      type = types.str;
      description = ''
        The default permissions to set for the key file, needs to be in the
        format accepted by <citerefentry><refentrytitle>chmod</refentrytitle>
        <manvolnum>1</manvolnum></citerefentry>.
      '';
    };
  });

in

{
  options = {
    keys = mkOption {
      default = {};
      type = types.attrsOf keyOptionsType;
    };
  };
  config = {
    systemd.services = (
      (flip mapAttrs' config.keys (name: keyCfg:
        nameValuePair "${name}-key" {
          enable = true;
          serviceConfig.TimeoutStartSec = "infinity";
          serviceConfig.Restart = "always";
          serviceConfig.RestartSec = "100ms";
          path = [ pkgs.inotifyTools ];
          preStart = ''
            mkdir -p $(dirname ${keyCfg.path})
            (while read f; do if [ "$f" = "${name}" ]; then break; fi; done \
              < <(inotifywait -qm --format '%f' -e create,move $(dirname ${keyCfg.path})) ) &

            if [[ -e "${keyCfg.path}" ]]; then
              echo 'flapped down'
              kill %1
              exit 0
            fi
            wait %1
          '';
          script = ''
            inotifywait -qq -e delete_self "${keyCfg.path}" &

            if [[ ! -e "${keyCfg.path}" ]]; then
              echo 'flapped up'
              exit 0
            fi
            wait %1
          '';
        }
      ))
    );
  };
}
