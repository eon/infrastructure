{ pkgs, config, sources, ...}:

with pkgs.lib;

let
  cfg = config;

  nivApps = with sources; [
    nextcloud-calendar
    nextcloud-contacts
  ];

  addNivApp = nivApp: ''
    ${pkgs.coreutils}/bin/cp -ar ${nivApp.outPath} $out/apps/${nivApp.name}
  '';

  addNivApps = concatStrings (map addNivApp nivApps);

  enableNivApps = map (app: ''
    nextcloud-occ app:enable ${app.name}
  '') nivApps;

  adminUsers = [ "lewo" "relaxmax" ];
  
  addUsers = let
    userPasswdPath = name: ''${cfg.keys."nextcloud-${name}".path}'';
    addUser = name: ''
      export OC_PASS=$(cat ${userPasswdPath name})
      ${pkgs.sudo}/bin/sudo -u nextcloud --preserve-env=OC_PASS -- \
        nextcloud-occ user:add \
          --group admin \
          --password-from-env \
          ${name}
      # If the user is already created, we may want to update its 
      # password.
      ${pkgs.sudo}/bin/sudo -u nextcloud --preserve-env=OC_PASS -- \
        nextcloud-occ user:resetpassword \
          --password-from-env \
          ${name}
      unset OC_PASS
      '';
  in concatMapStringsSep "\n" addUser adminUsers;

  # Generate key entries for all admin users
  # Each generated key looks like:
  #   keys.nextcloud-lewo = {
  #     path = "/run/keys/nextcloud-lewo";
  #     user = "nextcloud";
  #   };
  userKeys = let
    props = user: {
      path = "/var/keys/nextcloud-${user}";
      user = "nextcloud";
    };
    pairs = map (user: nameValuePair "nextcloud-${user}" (props user)) adminUsers;
  in listToAttrs pairs;

  # TODO: we need to add a package attribute to the nextcloud module instead.
  nextcloud = pkgs.nextcloud18.overrideAttrs(oldAttrs: rec {
    postFixup = ''
      ${addNivApps}
    '';
  });

in

{
  services.mysql = {
    enable = true;
    package = pkgs.mysql;
    ensureDatabases = [ "nextcloud" ];
    ensureUsers = [
      {
        name = "nextcloud";
        ensurePermissions = {
          "nextcloud.*" = "ALL PRIVILEGES";
        };
      }
    ];
  };

  keys = userKeys // {
    # This is the nextcloud admin password
    nextcloud  = {
      path = "/var/keys/nextcloud";
      user = "nextcloud";
    };
  };
  
  # The port 80 is required by acme
  networking.firewall.allowedTCPPorts = [ 443 80 ];

  services.nginx = {
    enable = true;
    virtualHosts = {
      "cloud.markas.fr" = {
        forceSSL = true;
        enableACME = true;
      };
    };
  };

  services.nextcloud = {
    enable = true;
    package = nextcloud;
    logLevel = 0;
    https = true;
    hostName = "cloud.markas.fr";
    nginx.enable = true;
    caching = {
      apcu = true;
    };
    config = {
      dbtype = "mysql";
      adminuser = "admin";
      adminpassFile = cfg.keys.nextcloud.path;
    };
  };

  # This is to do the initial nextcloud setup only when Mysql and
  # Redis are ready. We need to add this becaux mysql is on the same
  # host.
  systemd.services.nextcloud-setup = {
    serviceConfig.RemainAfterExit = true;
    partOf = [ "phpfpm-nextcloud.service" ];
    after = map (name: "nextcloud-${name}-key.service") adminUsers
            ++ [ "nextcloud-key.service" "mysql.service"];
    requires = map (name: "nextcloud-${name}-key.service") adminUsers
               ++ [ "nextcloud-key.service" "mysql.service" ];
    script = pkgs.lib.mkAfter ''
      ${addUsers}

      ${concatStringsSep "\n" enableNivApps}
      # This is required to enable apps :/
      nextcloud-occ upgrade
    '';
  };
}
