{ pkgs, config, ...}:

let 
  provisionning = pkgs.runCommand "grafana-provisioning" {} ''
    mkdir -p $out/{datasources,dashboards}

    cat <<EOF > $out/datasources/prometheus.yml
    apiVersion: 1
    datasources:
    - name: Prometheus
      type: prometheus
      access: proxy
      orgId: 1
      url: http://localhost:9090
      isDefault: true
      version: 1
      editable: false
    EOF

    cat <<EOF > $out/dashboards/dashboards.yml
    apiVersion: 1
    providers:
    - name: default
      orgId: 1
      folder: ""
      type: file
      disableDeletion: false
      updateIntervalSeconds: 0
      options:
        path: ${../conf/grafana}
    EOF
  '';
in
{
  networking.firewall = {
    extraCommands = ''
      # To allow VPN members to connect to the grafana service
      iptables -I INPUT -s 10.100.0.1/24 -p tcp --dport 3000 -i wg-member -j ACCEPT
    '';
  };
  services.grafana = {
    enable = true;
    addr = "0.0.0.0";
    analytics.reporting.enable = false;
    # It would be better to remove auth but I don't know how to create dashboard then.
    # We need to comment the line below to be able to create dashboards in the test VM
    auth.anonymous.enable = true;
    extraOptions = {
      PATHS_PROVISIONING = provisionning;
    };
  };
}
