{config, pkgs, ...}:

{
  # This adds specific options to run to server configuration in a VM.
  imports = [ ./configuration.nix ];
  config = {
    boot.loader.grub.device = "/dev/sda";
    fileSystems."/" = {
      device = "/dev/sda";
      fsType = "ext4";
    };

    users.extraUsers.root.password = "";

    # To allow root access via SSH
    services.openssh = {
      permitRootLogin = "yes";
      extraConfig = "PermitEmptyPasswords yes";
      passwordAuthentication = pkgs.lib.mkForce true;
    };

    # On the server, the anymous mode is enable. When the anonymous
    # mode is enable, it is not possible to create dashboard. In
    # the VM, we disable the authentication mode to allow
    # interactive dashboard creations.
    services.grafana.auth.anonymous.enable = pkgs.lib.mkForce false;

    # It takes too much resource when it starts in the VM, and it is
    # not really useful in the VM context.
    services.autoUpgrade.enable = pkgs.lib.mkForce false;

    networking.firewall.enable = pkgs.lib.mkForce false;

    # We don't have the VPN private key in a dev environment.
    services.vpn.member.enable = false;

    services.prometheus.alertmanager.logLevel = "debug";

    # We create values for expected Nextcloud passowords
    keys.nextcloud-lewo.path = pkgs.lib.mkForce (pkgs.writeTextFile {
      name = "nextcloud-lewo.password";
      text = "myLongPassword";
    });
    keys.nextcloud-relaxmax.path = pkgs.lib.mkForce (pkgs.writeTextFile {
      name = "nextcloud-relaxmax.password";
      text = "myLongPassword";
    });
    keys.nextcloud.path = pkgs.lib.mkForce ''${pkgs.writeTextFile {
      name = "nextcloud.password";
      text = "admin";
    }}'';

    virtualisation = {
      diskSize = 8 * 1024;
      memorySize = 1 * 1024;
      # To access the SSH server of the VM from your host
      qemu.networkingOptions = [
        "-net nic,netdev=user.0,model=virtio"
        "-netdev user,id=user.0,hostfwd=tcp::2222-:22,hostfwd=tcp::3000-:3000,hostfwd=tcp::9090-:9090,hostfwd=tcp::9100-:9100,hostfwd=tcp::9093-:9093,hostfwd=tcp::30443-:443"
      ];
    };
  };
}

