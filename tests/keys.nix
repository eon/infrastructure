{ pkgs }:

with import (pkgs.path + /nixos/lib/testing.nix) { system = builtins.currentSystem; };

let
  keyPath = "/run/keys/foo";
in

makeTest {
  name = "test-keys";
  nodes = {
    machine = {pkgs, config, ...}: {
      imports = [ ../modules/keys.nix ];
      config = {

       keys.foo.path = keyPath;

       systemd.services.test = {
          enable = true;
          wantedBy = [ "multi-user.target" ];
          after = [ "foo-key.service" ];
          wants = [ "foo-key.service" ];
          script = "cat /run/keys/foo > /tmp/foo-key-secret";
        };
      };
    };
  };
  # 1. Ensure foo-key.service is waiting for the key file existence
  # 2. Create this key file
  # 3. Ensure the depending service has been executed since it got the key
  testScript = ''
    $machine->waitUntilSucceeds("systemctl show foo-key.service -p ActiveState --value | grep activating -q");
    $machine->succeed("systemctl show test.service -p ActiveState --value | grep inactive -q");
    $machine->succeed("echo secret > ${keyPath}");
    $machine->waitUntilSucceeds("systemctl show foo-key.service -p ActiveState --value | grep active -q");
    $machine->waitUntilSucceeds("ls /tmp/foo-key-secret");
  '';
}
