{ pkgs, configuration }:

with import (pkgs.path + /nixos/lib/testing-python.nix) { system = builtins.currentSystem; };

let
  username = "lewo";
  password = "myLongPassword";
in

makeTest {
  name = "test-nextcloud";
  nodes = {
    machine = {pkgs, config, ...}: {
      imports = [ configuration ];
      config = {
        virtualisation = {
          diskSize = 1 * 1024;
          memorySize = 1 * 1024;
        };

        # Speedup the test by removing useless services
        services.autoUpgrade.enable = pkgs.lib.mkForce false;
        services.grafana.enable = pkgs.lib.mkForce false;

        # We create values for expected Nextcloud passowords
        keys."nextcloud-${username}".path = pkgs.lib.mkForce (pkgs.writeTextFile {
          name = "nextcloud.password";
          text = password;
        });
        keys."nextcloud-relaxmax".path = pkgs.lib.mkForce (pkgs.writeTextFile {
          name = "nextcloud.password";
          text = "myLongPassword";
        });
        keys.nextcloud.path = pkgs.lib.mkForce ''${pkgs.writeTextFile {
          name = "nextcloud.password";
          text = "admin";
        }}'';

        networking.extraHosts = "127.0.0.1 cloud.markas.fr";
      };
    };
  };

  testScript = let
    withRcloneEnv = pkgs.writeScript "with-rclone-env" ''
      #!${pkgs.stdenv.shell}
      export RCLONE_CONFIG_NEXTCLOUD_TYPE=webdav
      export RCLONE_CONFIG_NEXTCLOUD_URL="https://cloud.markas.fr/remote.php/webdav/"
      export RCLONE_CONFIG_NEXTCLOUD_VENDOR="nextcloud"
      export RCLONE_CONFIG_NEXTCLOUD_USER="admin"
      export RCLONE_CONFIG_NEXTCLOUD_PASS="$(${pkgs.rclone}/bin/rclone obscure admin)"
      "''${@}"
    '';
    copySharedFile = pkgs.writeScript "copy-shared-file" ''
      #!${pkgs.stdenv.shell}
      echo 'hi' | ${withRcloneEnv} ${pkgs.rclone}/bin/rclone --no-check-certificate rcat nextcloud:test-shared-file
    '';

    diffSharedFile = pkgs.writeScript "diff-shared-file" ''
      #!${pkgs.stdenv.shell}
      diff <(echo 'hi') <(${pkgs.rclone}/bin/rclone --no-check-certificate cat nextcloud:test-shared-file)
    '';
  in ''
    machine.wait_for_unit("multi-user.target")
    machine.succeed("curl -Lk https://cloud.markas.fr/login")

    machine.succeed(
        "${withRcloneEnv} ${copySharedFile}"
    )
    machine.succeed(
        "${withRcloneEnv} ${diffSharedFile}"
    )

    # TODO: check if calendar and contacts app are available
  '';
}
